/**
	CEU object
	Ivan Lausuch Sales
	4-11-2014	

	Values of user object:
	
	ceu.user.idUser
	ceu.user.id
	ceu.user.username
	ceu.user.name
	ceu.user.lastname
	ceu.user.email
	ceu.user.role
	ceu.user.status
	ceu.user.auth
	ceu.user.titulaciones
*/

var ceu = {
	//variable de usuario
	user:null,
	
	//Indica si ha habido algún error en el login
	loginError:false,
	
	//Indica el storename
	storeName:"ceu.general",
	
	//Función de showWait (Se puede sobrecargar)
	showWait:function(){
		mc.showWait();
	},
	
	//Función de hideWait (Se puede sobrecargar)
	hideWait:function(){
		mc.hideWait();
	},
	
	//Evento: Función que se ejecuta cuando el usuario ha sido validado (Se debe sobrecargar)
	onLoadUser:function(){},
	
	//Evento: Fuerza ir a la pantalla de login (Se puede sobrecargar)
	goToLogin:function(){
		mc.routeTo("login",true);
		mc.hideWait();
	},
	
	//Api url
	api:"http://wrapper.uchceu.es",
	
	//API error callback
	onApiError:function(message){
		console.error("API ERROR",message);
	},
	
	
	//Check if user is logged
	checkUser:function(callback){
		// If user is loaded don't load again
		if (ceu.user!=null){
			if (callback!=undefined)
				callback();
			else
				console.warn("ceu.js - checkUser - checkUser success but not callback defined");
				
			return;
		}
			
		ceu.showWait();
		
		var auth=null;
		
		//Try get method from URL
		var auth_method=getQueryVariable("auth_method");
		if (auth_method=="id"){
			
			var auth_id=getQueryVariable("auth_id");
			var auth_timestamp=getQueryVariable("auth_timestamp");
			var auth_signature=getQueryVariable("auth_signature");
			
			if (auth_id=="" || auth_timestamp=="" || auth_signature==""){
				ceu.goToLogin();
				return;
			}
			
			mc.$http.get(ceu.api+"/loginById", {params:{id:auth_id,timestamp:auth_timestamp,signature:auth_signature}})
				.success(function(data){
					console.info("ceu.js - checkUser - LoginById:", data);
					
					if (data.success==1){
						ceu.hideWait();
				    	ceu.setUser(data.data);
				    }
				    else{
					    ceu.login_error();
				    }
		    	})
		    	.error(function(data, status, headers, config) {
		    		ceu.login_error();
				});
			
			return;
		}
		
		//Try to get cookie
		try{
			auth=window.localStorage.getItem("ceu."+ceu.storeName+".user.auth");
			
			//If auth is valid
			if (auth!==undefined && auth!==null && auth!==""){
				console.info("ceu.js - checkUser - AUTH:",auth);
				ceu.auth(auth,callback);
			}else
				ceu.goToLogin();
		}
		catch(err){
			ceu.goToLogin();
			return;
		}
		
	},
	
	login_error:function(){
		ceu.hideWait();
		ceu.loginError=true;
		ceu.goToLogin();
	},
	
	//Login function
	login:function(login,password){
		ceu.showWait();
		ceu.loginError=false;
		
		login=login.toLowerCase();
		mc.$http.get(ceu.api+"/login", {params:{login:login,password:password}})
			.success(function(data){
				console.info("ceu.js - login - user data:",data);
				if (data.success==1){
					//Close waitwindow
			    	ceu.hideWait();
			    	
			    	//set user
			    	ceu.setUser(data.data);
			    	
			    	window.localStorage.setItem("ceu."+ceu.storeName+".user.login",login);
		    		window.localStorage.setItem("ceu."+ceu.storeName+".user.password",password);
	    		}
	    		else{
		    		ceu.login_error();
	    		}
	    	})
	    	.error(function(data, status, headers, config) {
	    		ceu.login_error();
			});
	},
	
	auth_error:function(){
		var login=window.localStorage.getItem("ceu."+ceu.storeName+".user.login");
		var password=window.localStorage.getItem("ceu."+ceu.storeName+".user.password");
		
		if (login==null || login==undefined || password==null || password==undefined){
			ceu.goToLogin();
		}else{
			mc.$http.get(ceu.api+"/login", {params:{login:login,password:password}})
			.success(function(data){
				if (data.success==1){
					//Close waitwindow
			    	ceu.hideWait();
			    	
					//set user
			    	ceu.setUser(data.data);
			    }
			    else{
				    ceu.login_error();
			    }
	    	})
	    	.error(function(data, status, headers, config) {
	    		ceu.login_error();
			});
				}
	},
	
	//Auth function
	auth:function(auth,callback){
		mc.$http.get(ceu.api+"/login", {params:{auth:auth}})
			.success(function(data){
				if (data.success==1){
					ceu.hideWait();
					ceu.setUser(data.data);	
					if (callback!=undefined)
						callback();
					else
						console.warn("ceu.js - auth - auth success but not callback defined");
				}
				else{
					ceu.auth_error();
				}
		    	
	    	})
	    	.error(function(data, status, headers, config) {
	    		ceu.auth_error();
			});
		
	},
	
	//Set user
	setUser:function(user){
		ceu.user=user;
		window.localStorage.setItem("ceu."+ceu.storeName+".user.auth", ceu.user.auth);
		ceu.onLoadUser();
	},
	
	//Logout
	logout:function(){
		ceu.user=null;
		window.localStorage.setItem("ceu."+ceu.storeName+".user.login","");
	    window.localStorage.setItem("ceu."+ceu.storeName+".user.password","");
	    window.localStorage.setItem("ceu."+ceu.storeName+".user.auth","");
	    
	    ceu.goToLogin();
	    
		return true;
	},
	
	//Get auth for calls to ws
	prepareHttpAuthParam:function(data){
		if (data==undefined)
			return {params:{auth:ceu.user.auth}};
		else{
			var obj=angular.copy(data);
			obj.auth=ceu.user.auth;
			return {params:obj};
		}
			
	},
	
	//Llamada al Wrapper del API
	callAPI:function(url,data,callback){
		mc.$http.get(ceu.api+url,{params:data})
			.success(function(result, status, headers, config) {
				if (result.success=="1")
	    			callback(result.data);
	    		else{
		    		ceu.onApiError(result.message)
	    		}
	    	})
	    	.error(function(result, status, headers, config) {
		    	ceu.onApiError(result)
		});
	},
	
	callAPI_WS2:function(ws,operation,data,callback){
		mc.$http.get(ceu.api+"/ws2/"+ws+"/"+operation+"/MOD_no_checkuser",{params:data})
			.success(function(result, status, headers, config) {
				if (result.success=="1")
	    			callback(result.data);
	    		else{
		    		ceu.onApiError(result.message)
	    		}
	    	})
	    	.error(function(result, status, headers, config) {
		    	ceu.onApiError(result)
		});
	}

}

function ceu_completeMainController(){
	mc.ceu=ceu;	
}